public with sharing class AccountTriggerHelper {

  private static boolean accountAfterTriggerHasFired = false;

  public static boolean getAccountAfterTriggerHasFired(){
    return accountAfterTriggerHasFired;
  }

  public static void setAaccountAfterTriggerHasFiredToTrue(){
    accountAfterTriggerHasFired = true;
  }

  public static void handleAfterUpdate(list<Account> newAccountList, map<id, Account> newAccountMap, map<Id, Account> oldAccountMap){
    //PART 1
    createOpportunitiesForAccounts(newAccountList);
    //PART 2
    updateContactMailingAddresses(newAccountList, newAccountMap, oldAccountMap);
  }


//TRIGGER PART 1
  public static void createOpportunitiesForAccounts(list<Account> newAccountList){
    list<Account> accountsToCreateOppsFor = getAccountsToCreateOppsFor(newAccountList);
    list<Opportunity> oppsToBeInserted = createOpportunitiesToBeInserted(accountsToCreateOppsFor);
    if(oppsToBeInserted.size()>0){insert oppsToBeInserted;}
  }

  public static list<Account> getAccountsToCreateOppsFor(list<Account> newAccountList){
    list<Account> accountsToCreateOppsFor = new list<Account>();
    for(Account a : newAccountList){
      if(a.Create_Opportunity__c && a.Number_Of_Opps__c==0){accountsToCreateOppsFor.add(a);}
    }
    return accountsToCreateOppsFor;
  }

  public static list<Opportunity> createOpportunitiesToBeInserted(list<Account> accountsToCreateOppsFor){
    list<Opportunity> oppsToBeInserted = new list<Opportunity>();
    Opportunity o;
    for(Account a : accountsToCreateOppsFor){
      o = new Opportunity();
      o.AccountId = a.id;
      o.Name = a.Name +' Opportunity';
      o.CloseDate = Date.today().addDays(Integer.valueOf(Label.CloseDateNumberOfDays));
      o.StageName = Label.StageName;
      //OTHER FIELDS ETC
      oppsToBeInserted.add(o);
    }
    return oppsToBeInserted;
  }
//END TRIGGER PART 1

//TRIGGER PART 2
  public static void  updateContactMailingAddresses(list<Account> newAccountList, map<id, Account> newAccountMap, map<Id, Account> oldAccountMap){
    list <Contact> contactsToUpdate = generateContactsToUpdate(newAccountList, oldAccountMap);
    if(contactsToUpdate!=null && contactsToUpdate.size()>0){
      Account a;
      for(Contact c : contactsToUpdate){
        a = newAccountMap.get(c.AccountId);
        c.MailingCity = a.BillingCity;
        c.MailingState = a.BillingState;
        c.MailingCountry = a.BillingCountry;
        c.MailingPostalCode = a.BillingPostalCode;
        c.MailingStreet = a.BillingStreet;
      }
      update contactsToUpdate;
    }
  }


  public static list<Contact> generateContactsToUpdate(list<Account> newAccountList, map<Id, Account> oldAccountMap){
    list<Id> accountIds = getAccountIdsWhoseBillingAddressHasChanged(newAccountList, oldAccountMap);
    return [select MailingCity, MailingState, MailingCountry, MailingPostalCode, MailingStreet, AccountId
            From Contact
            Where AccountId in :accountIds];
  }

  public static list<Id> getAccountIdsWhoseBillingAddressHasChanged(list<Account> newAccountList, map<Id, Account> oldAccountMap){
    list<Id> accountIds = new list<Id>();
    for(Account a : newAccountList){
      if(accountBillingAddressHasChanged(a, oldAccountMap.get(a.id))){
        accountIds.add(a.id);
      }
    }
    return accountIds;
  }

  public static boolean accountBillingAddressHasChanged(Account newAccount, Account oldAccount){
    
    return !((newAccount.BillingCity == oldAccount.BillingCity) && (newAccount.BillingState == oldAccount.BillingState)
             && (newAccount.BillingCountry == oldAccount.BillingCountry) && (newAccount.BillingPostalCode == oldAccount.BillingPostalCode)
             && (newAccount.BillingStreet == oldAccount.BillingStreet) );
  }
  //END TRIGGER PART 2
}