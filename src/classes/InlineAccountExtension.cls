public with sharing class InlineAccountExtension {
  private Id accountId;
  private Integer pageSize = 8;
  private ApexPages.StandardSetController setCont;

  public String searchVal {get; set;}

  public InlineAccountExtension(ApexPages.StandardController stdController) {
    accountId = stdController.getId();
    setupPage();
  }

  public void doSearch(){
    setupPage();
  }

  public void setupPage(){
    setCont = new ApexPages.StandardSetController(Database.getQueryLocator(getQuery()));
    setCont.setPageSize(pageSize);
  }

  public string getQuery(){
    String query = 'SELECT ';
    for(Schema.FieldSetMember f : SObjectType.Widget__c.FieldSets.InlineAccountPage.getFields()) {
      query += f.getFieldPath() + ', ';
    }
    query += 'Id FROM Widget__c WHERE Type__c = \'Sales\' ';
    
    if(String.isNotBlank(searchVal)){
      query+= 'AND Name like \'%'+searchVal+'%\' ';
    }
    query+= 'ORDER BY Name';
    return query;
  }

  public list<Widget__c> getRecords(){
    return (list<Widget__c>)setCont.getRecords();
  }

  public Boolean gethasNext() {
    return setCont.getHasNext();
  }
  
  public Boolean gethasPrevious() {
    return setCont.getHasPrevious();
  }
  
  public void first() {
    setCont.first();
  }
  
  public void last() {
    setCont.last();
  }
  
  public void previous() {
    setCont.previous();
  }
  
  public void next() {
    setCont.next();
  }
  
}