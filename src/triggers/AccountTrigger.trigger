trigger AccountTrigger on Account (before insert, before update, after insert, after update) {
  if(ActiveTrigger__c.getInstance('Account').active__c){
    if(trigger.isAfter && !AccountTriggerHelper.getAccountAfterTriggerHasFired()){
      AccountTriggerHelper.setAaccountAfterTriggerHasFiredToTrue();
      if(trigger.isUpdate){
        AccountTriggerHelper.handleAfterUpdate(trigger.new, trigger.newMap, trigger.oldMap);
      }
      if(trigger.isInsert){}
    }

  }

}